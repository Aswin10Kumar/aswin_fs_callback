const fs = require('fs');
const path = require('path');

function problem(numberOfFile, dirName, cb) {
    if (typeof cb != 'function') {
        console.log("cb is not a function");
    }
    else {
        try {

            const pathOfDir = path.join(__dirname, `./${dirName}`);

            const errLog = [];
            const sucessLog = [];

            fs.mkdir(pathOfDir, { recursive: true }, (err) => {
                if (err) {
                    cb(err);
                }
                else {
                    for (let count = 1; count <= numberOfFile; count++) {
                        const filePath = path.join(__dirname, `./${dirName}/file${count}.json`)

                        fs.writeFile(filePath, '', (err) => {
                            if (err) {
                                errLog.push(err);
                            }
                            else {
                                sucessLog.push(`created file ${filePath}`)
                            }

                            if (count === numberOfFile) {
                                fs.readdir(pathOfDir, 'utf-8', callUnlink);
                            }
                        })
                    }
                }
            })

            function callUnlink(err, files) {
                // console.log(files);
                if (err) {
                    errLog.push(err);
                } else {
                    if (files.length == 0) {
                        errLog.push('No files available to delete')
                        cb(errLog, sucessLog);
                    }
                    else {
                        for (let count = 0; count < numberOfFile; count++) {
                            setTimeout(() => {
                                const filePath = path.join(__dirname, `./${dirName}/${files[count]}`)

                                fs.unlink(filePath, (err) => {
                                    if (err) {
                                        errLog.push(err);
                                    }
                                    else {
                                        sucessLog.push(`Deleted file ${filePath}`)
                                    }

                                    if (count === numberOfFile - 1) {
                                        cb(errLog, sucessLog);
                                    }
                                })
                            }, 10000)

                        }
                    }
                }

            }

        }
        catch (error) {
            cb(error);
        }
    }
}

module.exports = problem;