/* Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously
*/

const problem1 = require('../problem1')

const dirName = 'JSON_files';
const numberOfFiles = 5;

problem1(numberOfFiles, dirName, executionStatus);

function executionStatus(fail, sucess) {

    console.log("\nERROR:\n")
    if (fail.length !== 0) {
        console.log(fail.join("\n"));
    }

    console.log("\nWORK DONE:\n")
    if (sucess.length !== 0) {
        console.log(sucess.join("\n"));
    }
}