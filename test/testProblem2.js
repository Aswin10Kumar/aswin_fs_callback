const problem2 = require('../problem2')
const fs = require('fs')
const path = require('path');

const fileToRead = "./data/lipsum.txt";
const mainFileName = "filenames.txt"

const mainFile = path.join(`${__dirname},/../${mainFileName}`)
// let writer = fs.createWriteStream(mainFile)
// writer.write('', () => {
fs.writeFile(mainFile,'',()=>{
    problem2(mainFileName, fileToRead, (err, data) => {

        console.log("ERROR: \n")
        console.log(err.join('\n'));

        console.log("\nSUCCESS: \n")
        console.log(data.join('\n'));

    })
})
