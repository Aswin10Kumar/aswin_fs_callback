/*
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

function problem2(mainFileName, fileToRead, cb) {
    if (typeof cb != 'function') {
        console.log("cb is not a function")
    }
    else {
        try {
            readFiles(fileToRead, convertToUpper);

            const errLog = [];
            const successLog = [];

            function convertToUpper(err, data) {
                if (err != null) {
                    errLog.push(err)
                } else {
                    const convertedData = data.toUpperCase();
                    writeFiles('upperCase.txt', mainFileName, convertedData, (err, fileName) => {
                        if (err) {
                            errLog.push(err);
                        }
                        else {
                            successLog.push(`upperCase.txt is created`);
                            readFiles(fileName, convertToSentence)
                        }
                    })
                }
            }

            function convertToSentence(err, data) {
                if (err != null) {
                    deleting(mainFileName);
                    errLog.push(err)
                } else {
                    let convertedData = data.toLowerCase().replace(/\. /g, ".\n");

                    writeFiles('sentence.txt', mainFileName, convertedData, (err, fileName) => {
                        if (err) {
                            deleting(mainFileName);
                            errLog.push(err)
                        }
                        else {
                            successLog.push('sentence.txt is created');
                            readFiles(fileName, convertToLower)
                        }
                    })
                }
            }

            function convertToLower(err, data) {
                if (err) {
                    deleting(mainFileName);
                    errLog.push(err)
                }
                else {
                    const seperateWords = data.split(/['' |'\n' |',' |'.']/g)
                    const sortedData = seperateWords.sort((a, b) => a.localeCompare(b)).join('\n');
                    const removeEmptyLine = sortedData.replace(/^\s/gm, "");

                    writeFiles('sortedWords.txt', mainFileName, removeEmptyLine, (err, fileName) => {
                        if (err) {
                            deleting(mainFileName);
                            errLog.push(err)
                        }
                        else {
                            successLog.push('sortedWords.txt is created');
                            setTimeout(() => { deleting(mainFileName); }, 10000)
                        }
                    })
                }
            }

            function deleting(mainFile) {
                readFiles(`${mainFile}`, (err, data) => {
                    if (err != null) {
                        errLog.push(err)
                    }
                    else {
                        successLog.push('filenames.txt data obtained');
                        const fileInArray = data.split('\n');

                        for (let index = 1; index < fileInArray.length; index++) {
                            const filePath = path.join(__dirname, fileInArray[index - 1])
                        
                            fs.unlink(filePath, err => {
                                if (err) {
                                    errLog.push(err);
                                }else{
                                    successLog.push(`${fileInArray[index - 1]} is deleted`);
                                }
                                if (index == fileInArray.length - 1) {

                                    cb(errLog, successLog)
                                }
                            })
                        }
                    }
                })
            }
        }
        catch (err) {
            cb(err.message);
        }
    }

}

function writeFiles(fileName, mainFileName, content, cb) {
    const newPath = path.join(__dirname, `./${fileName}`)

    fs.writeFile(newPath, content, err => {
        if (err) {
            cb(err)
        }
        else {
            const newPath = path.join(__dirname, `./${mainFileName}`)

            fs.appendFile(newPath, `${fileName}\n`, err => {
                if (err) {
                    errLog.push(err)
                }
                else {
                    cb(null, `${fileName}`)
                }
            })
        }
    })
}

function readFiles(fileName, cb) {
    const newPath = path.join(__dirname, `./${fileName}`)

    fs.readFile(newPath, 'utf-8', (err, data) => {
        if (err) {
            cb(err)
        } else {
            cb(null, data);
        }
    })
}

module.exports = problem2;